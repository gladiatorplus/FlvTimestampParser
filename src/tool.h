#pragma once
static const char* getAudioTagSoundFormat(const unsigned int format){
    switch(format){
        case 0:
            return "Linear PCM, platform endian";
        break;
        case 1:
            return "ADPCM";
        break;
        case 2:
            return "MP3";
        break;
        case 3:
            return "Linear PCM, little endian";
        break;
        case 4:
            return "Nellymoser 16-kHz mono";
        break;
        case 5:
            return "Nellymoser 8-kHz mono";
        break;
        case 6:
            return "Nellymoser";
        break;
        case 7:
            return "G.711 A-law logarithmic PCM";
        break;
        case 8:
            return "G.711 mu-law logarithmic PCM";
        break;
        case 9:
            return "reserved";
        break;
        case 10:
            return "AAC";
        break;
        case 11:
            return "Speex";
        break;
        case 14:
            return "MP3 8-Khz";
        break;
        case 15:
            return "Device-specific sound";
        break;
        default:
            return "UNKNOWN";
        break;
    }
}
static const char* getAudioTagSoundRate(const unsigned int rate){
    switch(rate){
        case 0:
            return "5.5 kHz";
        break;
        case 1:
            return "1 kHz";
        break;
        case 2:
            return "22 kHz";
        break;
        case 3:
            return "44 kHz";
        break;
        default:
            return "UNKNOWN";
        break;
    }
}

static const char* getVideoTagFrameType(const unsigned int frameType){
    switch(frameType){
        case 0:
            return "UNKNOWN";
        break;
        case 1:
            return "keyframe";
        break;
        case 2:
            return "inter frame";
        break;
        case 3:
            return "disposale inter frame";
        break;
        case 4:
            return "generated keyframe";
        break;
        case 5:
            return "video info/command frame";
        break;
        default:
            return "UNKNOWN";
        break;
    }
}

static const char* getVideoTagCodecID(const unsigned int ID){
    switch(ID){
        case 0:
            return "UNKNOWN";
        break;
        case 1:
            return "JPEG";
        break;
        case 2:
            return "Sorenson H.263";
        break;
        case 3:
            return "Screen video";
        break;
        case 4:
            return "On2 VP6";
        break;
        case 5:
            return "On2 VP6 with alpha channel";
        break;
        case 6:
            return "Screen video version 2";
        break;
        case 7:
            return "AVC";
        break;
        default:
            return "UNKNOWN";
        break;
    }
}

static const char* getAVCPacketType(const unsigned int type){
    switch(type){
        case 0:
            return "AVC sequence header";
        break;
        case 1:
            return "AVC NALU";
        break;
        case 2:
            return "AVC end of sequence";
        break;
        default:
            return "UNKNOWN";
        break;
    }
}