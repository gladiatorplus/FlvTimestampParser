#pragma once
#include <cstdio>
#include <fstream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <functional>

static const int BufferSize = 500*1024;
static const int PieceSize = 1*1024;

struct commonElements
{
    std::vector<char> buffer;//数据缓冲区
    std::vector<char> piece;//数据读取
    int readPos;//读取位置,还没读取
    int writePos;//已经写入位置
    int readLen;//读取数据的长度
    bool isFirst;//第一次处理
    int swtichThead;//读写pos相同时线程切换处理
    long long int  fileLength;//文件长度，读取文件完成时退出consumer线程
    std::mutex mtx;
    std::condition_variable bufferNotFull;//表示缓冲区不为满
    std::condition_variable bufferNotEmpty;//表示缓冲区不为空
    commonElements(){
        buffer = std::vector<char>(BufferSize);
        piece = std::vector<char>(PieceSize);
        isFirst = true;
        readPos = 0;
        writePos = 0;
        readLen = 9;//默认为flvheader的长度
        swtichThead = 0;
        fileLength = 0;
    }
};

static commonElements pConmmonElements;

enum ErrType{
        OK,
        ERR_EOF,
        ERR_CONNOT_OPEN_FILE,
        ERR_CONNOT_OPEN_FILE_STREAM,
        ERR_INVALID_FILE,
    };

class Parser{
public:
    Parser();
    virtual ~Parser();
private:
    std::ifstream m_fs;//文件流
     /**
     * @param pCommonElements 线程间共享数据 
     * @param ifs 打开的文件流
     * @return 返回-1,表示打开失败 
     */
    static void Producer(void * pCommonElements,std::ifstream& ifs);

     /**
     * @param pCommonElements 线程间共享数据 
     * @param call 取到数据后的回调函数
     * @return 返回-1,表示打开失败 
     */
    static void Consumer(void * pCommonElements,std::function<int(std::vector<char>,int)> call);
protected:
    std::function<int(std::vector<char> ,int a)> m_call;//取到数据的回调函数

    /**
     * @param path 文件路径 
     * 
     * @return 返回-1,表示打开失败 
     */
    int openFile(const char* path);

    /**
     * @param 
     * 
     * @return 初始化读写线程
     */
    void Init();

    /**
     * @param 
     * 
     * @return 返回是否读取完成
     */
    bool isEOF();

    /**
     * @param path 文件路径
     * 
     * @return 返回是否读取完成
     */
    unsigned long long getFileLength(const char* path);

    /**
     * @param type 自定义错误类型 
     * @param info 额外信息
     * @return 
     */
    void printInfo(ErrType type,const char* info="\n");
};
