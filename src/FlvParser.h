#pragma once
#include <cstring>
#include "Parser.h"

typedef unsigned char BT;
static unsigned int AUDIOTYPE = 0x08;
static unsigned int VIDEOTYPE = 0x09;
static unsigned int SCRIPTTYPE = 0x12;

struct FlvHeader{
    BT flvSig[3];
    BT version;
    BT TypeFlag;
    BT DataOffset[4];

    FlvHeader(){
        memset(this,0,sizeof(FlvHeader));
    }
};

struct TagHeader{
    BT previousTagSize[4];
    BT TagType;
    BT DataSize[3];
    BT TimeStamp[3];
    BT TimeStampExtended;
    BT StreamID[3];

    TagHeader(){
        memset(this,0,sizeof(TagHeader));
    }
};

class FlvParser:public Parser{
private:
    unsigned int m_preTagType;
    bool m_bFlvHeader;
    bool m_bTagHeader;//上一个解析数据是否为tagheader

    /**
     * @param buf 读取的缓存
     * 
     * @return 解析FLV header
     */
    void parseFLVHeader(const char* buf);

    /**
     * @param buf 读取的缓存
     * 
     * @return 解析FLV tag header
     */
    int parseFlvTagHeader(const char* buf);

    /**
     * @param buf 读取的缓存
     * 
     * @return 解析FLV tag data
     */
    void parseFlvTagData(const char* buf);

    /**
     * @param 
     * 
     * @return 
     */
    int parseFlvData(std::vector<char> piece,int a);

    /**
     * @param buf 读取的缓存
     * 
     * @return 
     */
    void doAudioTagData(const char* buf);
    
    /**
     * @param buf 读取的缓存
     * @param buffLen 缓存大小
     * @return 
     */
    void doVideoTagData(const char* buf);
    void doVideoData(const char* buf);

public:
    explicit FlvParser(const char* path);
    ~FlvParser();

    /**
     * @param 
     * 
     * @return 解析数据入口
     */
    void parseTag();
};

