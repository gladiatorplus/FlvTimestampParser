#include <iostream>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "Parser.h"

Parser::Parser(){

}

Parser::~Parser(){
    if(m_fs.is_open())
        m_fs.close();
}

int Parser::openFile(const char* path){
    pConmmonElements.fileLength = getFileLength(path);
    m_fs.open(path,std::ios::in);
    if(!m_fs.is_open()){
        printInfo(ErrType::ERR_CONNOT_OPEN_FILE,path);
        return -1;
    }
    return 0;
}

unsigned long long Parser::getFileLength(const char* path){
    FILE * fp = fopen(path , "r");
    if(nullptr == fp){
        printInfo(ErrType::ERR_CONNOT_OPEN_FILE,path);
        return 0;
    }

    fseek(fp,0,SEEK_END);
    unsigned long long length = ftell(fp);
    fseek(fp,0,SEEK_SET);

    if(0 == length){
        printInfo(ErrType::ERR_INVALID_FILE,path);
        fclose(fp);
        return 0;
    }

    fclose(fp);
    return length;
}

void Parser::Init(){
    std::thread producer(&Parser::Producer,(void*)&pConmmonElements,std::ref(this->m_fs));
    std::thread consumer(&Parser::Consumer,(void*)&pConmmonElements,std::ref(this->m_call));
    producer.join();
    consumer.join();
}

void Parser::Producer(void * pCommonEles,std::ifstream& ifs){
    commonElements* conElements = (commonElements*)pCommonEles;
    while(!ifs.eof()){
        std::unique_lock<std::mutex> lock(conElements->mtx);

        while(!conElements->isFirst){
            //读数据
            unsigned int emptySize = (conElements->writePos < conElements->readPos) ? 
                    (conElements->readPos - conElements->writePos) : abs(BufferSize + conElements->readPos - conElements->writePos);

            //临界点
            if(conElements->writePos == BufferSize && emptySize == BufferSize){
                conElements->writePos = 0;
            }

            int nextWritePos = conElements->writePos + emptySize;

            bool isWait = false;
            if(conElements->readPos == conElements->writePos){
                isWait = true;
                if(conElements->swtichThead == 2)
                {
                    isWait =false;
                }
            }else if(conElements->readPos < conElements->writePos){
                if(nextWritePos > BufferSize){
                    nextWritePos -= BufferSize;
                    if(nextWritePos > conElements->readPos){
                        isWait = true;
                    }
                }
            }else{
                if(nextWritePos > conElements->readPos){
                    isWait = true;
                    nextWritePos %= BufferSize;
                }
            }

            conElements->swtichThead = 1;

            if(isWait || emptySize == 0){
                printf("Producer is waiting for an empty slot...\n");
                (conElements->bufferNotFull).wait(lock);
            }else{
                break;
            }
        }

        if(!ifs.is_open() /*|| nullptr == buf*/){
            printf("read file Erro...\n");
            return ;
        }

        unsigned int newEmptySize = (conElements->writePos < conElements->readPos) ? 
                    (conElements->readPos - conElements->writePos) : abs(BufferSize + conElements->readPos - conElements->writePos);

        //读数据
        if(conElements->writePos + newEmptySize <= BufferSize)
            ifs.read(&conElements->buffer[0] + conElements->writePos,newEmptySize);
        else{  //读到文件末尾处理
            ifs.read(&conElements->buffer[0] + conElements->writePos,BufferSize - conElements->writePos);
            ifs.read(&conElements->buffer[0],newEmptySize - BufferSize + conElements->writePos);
        }

        conElements->writePos = conElements->writePos + newEmptySize;
        if(conElements->writePos > BufferSize){
            conElements->writePos %= BufferSize;
        }

        //读到buffer最后时，避免重复读
        if(conElements->readPos == BufferSize && newEmptySize == BufferSize){
            conElements->readPos = 0;
        }

        //唤醒consumer
        (conElements->bufferNotEmpty).notify_all();

        lock.unlock();
    }
    printf("Producer end!!!!!\n");
}

void Parser::Consumer(void* pCommonEles,std::function<int(std::vector<char>,int)> call){
    commonElements* conElements = (commonElements*)pCommonEles;
    while(1){
        std::unique_lock<std::mutex> lock(conElements->mtx);
        while(!conElements->isFirst){
            bool isWait = false;
            int nextReadPos = conElements->readPos + conElements->readLen;
            if(conElements->readPos == conElements->writePos){
                isWait = true;
                if(conElements->swtichThead == 1)
                {
                    isWait =false;
                }
            }else if(conElements->readPos < conElements->writePos){
                if(nextReadPos > conElements->writePos){
                    isWait = true;
                    nextReadPos %= BufferSize;
                }
            }else{
                if(nextReadPos > BufferSize){
                    nextReadPos -= BufferSize;
                    if(nextReadPos > conElements->writePos){
                        isWait = true;
                    }
                }
            }

            conElements->swtichThead = 2;

            if(isWait){
                printf("Consumer is waiting for data...\n");
                (conElements->bufferNotEmpty).wait(lock);
            }else{
                break;
            }
        }

        //取数据
        std::vector<char>::const_iterator start = conElements->buffer.begin() + conElements->readPos; 
        int pos = conElements->readPos + conElements->readLen;//conElements->piece.size()
        conElements->piece.clear();
        // memset(&conElements->piece,0,conElements->piece.capacity());

        if(pos >= BufferSize){//buffer 末尾
            std::vector<char>::const_iterator end = conElements->buffer.end();
            conElements->piece.insert(conElements->piece.begin(),start,end);
            end = conElements->buffer.begin() + pos - BufferSize;
            start =  conElements->buffer.begin();
            conElements->piece.insert(conElements->piece.begin() + (BufferSize - conElements->readPos), start, end);
        }else{
            std::vector<char>::const_iterator end = conElements->buffer.begin() + pos;
            conElements->piece.insert(conElements->piece.begin(),start, end); 
        }

        conElements->readPos = conElements->readPos + conElements->readLen;

        if(conElements->readPos > BufferSize){
            conElements->readPos %= BufferSize;
        }

        //文件被全部读取，退出读线程
        conElements->fileLength -= conElements->readLen;
        if(conElements->fileLength < 0){
            break;
        }

        //下一次的readlen
        conElements->readLen = call(std::ref(conElements->piece),conElements->readLen);
        
        conElements->isFirst = false;
        //解析数据
        (conElements->bufferNotFull).notify_all();

        lock.unlock();
    }
    printf("Consumer end!!!!!\n");
}

bool Parser::isEOF(){
    return m_fs.eof();
}

void Parser::printInfo(ErrType type,const char* info){
    std::string message = "";
    switch (type)
    {
    case OK:
        
    break;
    case ERR_EOF:
        message = "Read file finish\n";
        break;
    case ERR_CONNOT_OPEN_FILE:
        message = "Failed to open file:%s\n";
        break;
    case ERR_CONNOT_OPEN_FILE_STREAM:
        message = "open flv file failed or buffer not init!!!!!!";
        break;
    case ERR_INVALID_FILE:
        message = "Invalid file:%s\n";
        break;
    default:
        break;
    }

    printf(message.c_str(),info);
}