#include <iostream>
#include <string.h>
#include <vector>
#include "FlvParser.h"
#include "tool.h"

FlvParser::FlvParser(const char* path):m_bFlvHeader(true),m_bTagHeader(true){
    openFile(path);
}

FlvParser::~FlvParser(){
    printf("FlvParser destruct....\n");
}

void FlvParser::parseFLVHeader(const char* buf){
    FlvHeader flvHeader;
    memcpy(&flvHeader,buf,sizeof(FlvHeader));
    printf("version: %d,Type:%d\n",(unsigned int)flvHeader.version,(unsigned int)flvHeader.TypeFlag);
}

const char* getTagType(const unsigned int type){
    if(AUDIOTYPE == type){
        return "audio";
    }else if(VIDEOTYPE == type){
        return "video";
    }else if(SCRIPTTYPE == type){
        return "script";
    }else{
        return "";
    }
}

int FlvParser::parseFlvTagHeader(const char* buf){
    TagHeader tagHeader;
    memcpy(&tagHeader,buf,sizeof(TagHeader));

    BT extendTimeStamp = tagHeader.TimeStampExtended;
    unsigned int previousTagSize = (tagHeader.previousTagSize[0]) << 24 | (tagHeader.previousTagSize[1] << 16) |
                            (tagHeader.previousTagSize[2] << 8) | tagHeader.previousTagSize[3];
    unsigned int dataSize = (tagHeader.DataSize[0] << 16) | (tagHeader.DataSize[1] << 8) | tagHeader.DataSize[2];
    unsigned int timeStamp = (extendTimeStamp << 24) | (tagHeader.TimeStamp[0] << 16) | (tagHeader.TimeStamp[1] << 8) |
                        (tagHeader.TimeStamp[2]);
    printf("Tag with timestamp %d,datasize %d of type %s.\n\n",timeStamp,dataSize,getTagType(tagHeader.TagType));
    m_preTagType = tagHeader.TagType;
    m_bTagHeader = false;
    return dataSize;
}

void FlvParser::parseFlvTagData(const char* buf){
    m_bTagHeader = true;
    if(VIDEOTYPE == m_preTagType){
        doVideoTagData(buf);
    }else if(AUDIOTYPE == m_preTagType){
        doAudioTagData(buf);
    }else if(SCRIPTTYPE == m_preTagType){

    }else{

    }
}

void FlvParser::parseTag(){
    m_call = std::bind(&FlvParser::parseFlvData, this,std::placeholders::_1,std::placeholders::_2);
    Init();
}
int FlvParser::parseFlvData(std::vector<char> piece,int a){   
    char* buffer = &piece[0];
    int readLen = a;
    memcpy(buffer,buffer,readLen);
    if(readLen == sizeof(FlvHeader) && m_bFlvHeader){
        parseFLVHeader(buffer);
        m_bFlvHeader = false;
        //读取下一个
        readLen = sizeof(TagHeader);
    }else if(readLen == sizeof(TagHeader) && m_bTagHeader){
        int dataSize = parseFlvTagHeader(buffer);
        //更新读取长度
        readLen = dataSize;
    }else{//解析tag数据
        parseFlvTagData(buffer);
        readLen = sizeof(TagHeader);
    }
    return readLen;
}

void FlvParser::doAudioTagData(const char* buf){
    BT btInfo = buf[0];
    unsigned int  soundFormat = btInfo >> 4;
    unsigned int soundRate = (btInfo >> 2) & 0x03;
    unsigned int soundSize = (btInfo >> 1) & 0x02;
    unsigned int soundType = (btInfo & 0x01);
    
    printf("--------audio tag------\n");
    printf("SoundFormat     SoundRate   SoundSize   SoundType   AACPacketType\n");
    printf("   %s               %s         %s            %s         ",getAudioTagSoundFormat(soundFormat),
            getAudioTagSoundRate(soundRate),soundSize ? "snd16Bit":"snd8Bit",soundType ? "sndStereo":"sndMono");

    //是AAC数据则打印信息
    if(10 == soundFormat){
        BT audioPacketType = buf[1];
        unsigned int aacPacketType = audioPacketType;
        printf("%s\n",aacPacketType ? "AAC raw" : "AAC sequence header");
    }    

    printf("--------audio tag------\n\n");
}

void FlvParser::doVideoTagData(const char* buf){
    BT btInfo = buf[0];
    unsigned int frameType = btInfo >> 4;
    unsigned int codecID = btInfo & 0x0F;

    BT videoPacketType = buf[1];
    unsigned int avcPacketType = videoPacketType;
    unsigned int compositionTime = (buf[2] << 16) | (buf[3] << 8) | (buf[4]);

    printf("--------video tag------\n");
    if(7 == codecID){
        printf("FrameType     CodecID   AVPacketType   CompositionTime\n");
        printf(" %s       %s     %s       %d\n",getVideoTagFrameType(frameType),
            getVideoTagCodecID(codecID),getAVCPacketType(avcPacketType),compositionTime);
    }else{
        printf("FrameType     CodecID\n");
        printf(" %s       %s    \n",getVideoTagFrameType(frameType),
            getVideoTagCodecID(codecID));
    }

    //视频数据解析
    printf("=video tag data=\n");

    printf("--------video tag------\n\n");
}

void FlvParser::doVideoData(const char* buf){
    
}
