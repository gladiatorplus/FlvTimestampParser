#include <iostream>
#include <cstdio>
#include <string.h>
#include <fstream>
#include "FlvParser.h"

int main(){
    const char* path = "sample.flv";
    FlvParser flvParser(path);

    flvParser.parseTag();
    
    printf("main thread is end...\n");
    return 0;
}